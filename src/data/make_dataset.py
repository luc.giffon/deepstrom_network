# -*- coding: utf-8 -*-

import click
import tempfile
import numpy as np
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from keras.datasets import cifar10, cifar100, mnist

from skluc.main.utils import read_matfile, logger, download_data


def load_svhn_data():
    data_root_url = "http://ufldl.stanford.edu/housenumbers/"
    data_leaf_values = {
            "train": "train_32x32.mat",
            "test": "test_32x32.mat",
    }
    data_arrays = {}

    with tempfile.TemporaryDirectory() as d_tmp:
        for leaf_name, leaf in data_leaf_values.items():
            leaf_url = data_root_url + leaf
            logger.debug(f"Downloading file from url {leaf_url} to temporary directory {d_tmp}")
            matfile_path = download_data(leaf_url, d_tmp, leaf)
            data_arrays[leaf_name] = read_matfile(matfile_path)

    return data_arrays["train"], data_arrays["test"]


MAP_NAME_DATASET = {
    "cifar10": cifar10.load_data,
    "cifar100fine": lambda: cifar100.load_data(label_mode="fine"),
    "mnist": mnist.load_data,
    "svhn": load_svhn_data
}

def _download_all_data(output_dirpath):
    for key, _ in MAP_NAME_DATASET.items():
        _download_single_dataset(output_dirpath, key)

def _download_single_dataset(output_dirpath, dataname):
    (x_train, y_train), (x_test, y_test) = MAP_NAME_DATASET[dataname]()
    map_savez = {"x_train": x_train,
                 "y_train": y_train,
                 "x_test": x_test,
                 "y_test": y_test
                 }
    output_path = project_dir / output_dirpath / dataname
    logger.info(f"Save {dataname} to {output_path}")
    np.savez(output_path, **map_savez)

@click.command()
@click.argument('dataset', default="all")
@click.argument('output_dirpath', type=click.Path())
def main(output_dirpath, dataset):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    if dataset == "all":
        _download_all_data(output_dirpath)
    else:
        _download_single_dataset(output_dirpath, dataset)


if __name__ == '__main__':
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
