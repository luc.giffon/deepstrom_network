import time as t

import numpy as np
import tensorflow as tf
import keras
from keras.callbacks import LearningRateScheduler
from keras.layers import Dense, BatchNormalization, Flatten, Lambda, Input, Lambda, concatenate
from keras.optimizers import SGD, Adam
from keras.preprocessing.image import ImageDataGenerator
import skluc.main.data.mldatasets as dataset
from skluc.main.data.utils import normalize, get_uniform_class_rand_indices
from skluc.main.keras_.kernel import map_kernel_name_function
from skluc.main.tensorflow_.kernel_approximation.fastfood_layer import FastFoodLayer
from skluc.main.tensorflow_.kernel import tf_linear_kernel, tf_rbf_kernel, tf_chi_square_CPD, tf_chi_square_CPD_exp
from skluc.main.tensorflow_.utils import batch_generator
from skluc.main.utils import logger, memory_usage, ParameterManager, ResultManager, ResultPrinter, create_directory
from src import project_dir
from sklearn.model_selection import train_test_split
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.initializers import he_normal
from skluc.main.keras_.kernel import map_kernel_name_function


from src.features.build_features import _get_model_from_file




def build_sequential_model(input_shape):
    # build model
    model = Sequential()
    weight_decay = 0.0001

    # Block 1
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block1_conv1', input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block1_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'))

    # Block 2
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block2_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block2_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'))

    # Block 3
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'))

    # Block 4
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'))

    # Block 5
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool'))

    # model modification for cifar-10
    return model

def main():
    def init_number_subsample_bases():
        '''return: nb_sub_base, zero_padding'''
        remaining = nys_size % batch_size
        quotient = nys_size // batch_size
        if nys_size == 0 or batch_size == 0:
            raise ValueError
        if remaining == 0:
            return quotient, 0
        elif quotient == 0:
            return 1, batch_size - remaining
        else:
            return quotient + 1, batch_size - remaining

    batch_size = 128
    epochs = 100
    num_classes = 10
    nys_size = 32
    nb_subsample_bases, zero_padding_base = init_number_subsample_bases()
    kernel = "chi2_cpd"
    kernel_dict = {}
    cifar10_data_dir = project_dir / "data/external" / "cifar10.npz"
    loaded_npz = np.load(cifar10_data_dir)
    (x_train, y_train), (x_test, y_test) = (loaded_npz["x_train"], loaded_npz["y_train"]), (loaded_npz["x_test"], loaded_npz["y_test"])

    # convmodel_func = _get_model_from_file("vgg19", "cifar10", "block5_pool")
    convmodel_func = build_sequential_model(x_train[0].shape)

    datagen = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=10000, random_state=0)

    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_val = keras.utils.to_categorical(y_val, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)
    x_train = x_train.astype('float32')
    x_val = x_val.astype('float32')
    x_test = x_test.astype('float32')
    y_train = y_train.astype('float32')
    y_val = y_val.astype('float32')
    y_test = y_test.astype('float32')
    x_train = normalize(x_train)  # type: np.ndarray
    x_val = normalize(x_val)  # type: np.ndarray
    x_test = normalize(x_test)  # type: np.ndarray

    datagen.fit(x_train)

    subsample_indexes = get_uniform_class_rand_indices(y_train, nys_size)
    nys_subsample = x_train[subsample_indexes]
    zero_padding_subsample = np.zeros((zero_padding_base, *nys_subsample.shape[1:]))
    nys_subsample = np.vstack([nys_subsample, zero_padding_subsample])
    list_subsample_bases = [nys_subsample[i * batch_size:(i + 1) * batch_size] for i in range(nb_subsample_bases)]

    # # Model definition

    input_dim = x_train.shape[1:]
    output_dim = y_train.shape[1]

    convmodel_func.add(Flatten())

    input_x = Input(shape=input_dim, name="x")

    repr_x = convmodel_func(input_x)

    input_repr_subsample = [Input(batch_shape=(batch_size, *input_dim)) for _ in range(nb_subsample_bases)]
    if nb_subsample_bases > 1:
        input_subsample_concat = concatenate(input_repr_subsample, axis=0)
    else:
        input_subsample_concat = input_repr_subsample[0]

    slice_layer = Lambda(lambda input: input[:nys_size],
                         output_shape=lambda shape: (nys_size, *shape[1:]))
    input_subsample_concat = slice_layer(input_subsample_concat)
    reprs_subsample = convmodel_func(input_subsample_concat)

    if kernel == "linear":
        kernel_function = lambda *args, **kwargs: map_kernel_name_function["linear"](*args, **kwargs, normalize=True, **kernel_dict)
    elif kernel == "rbf":
        kernel_function = lambda *args, **kwargs: map_kernel_name_function["rbf"](*args, **kwargs, tanh_activation=True, normalize=True, **kernel_dict)
    elif kernel == "chi2_cpd":
        kernel_function = lambda *args, **kwargs: map_kernel_name_function["chi2_cpd"](*args, **kwargs, epsilon=1e-8, tanh_activation=True, normalize=True, **kernel_dict)
    elif kernel == "chi2_exp_cpd":
        kernel_function = lambda *args, **kwargs: map_kernel_name_function["chi2_exp_cpd"](*args, **kwargs, epsilon=1e-8, tanh_activation=True, normalize=True, **kernel_dict)
    else:
        raise NotImplementedError(f"unknown kernel function {kernel}")

    kernel_layer = Lambda(kernel_function, output_shape=lambda shapes: (shapes[0][0], nys_size))
    kernel_vector = kernel_layer([repr_x, reprs_subsample])

    input_classifier = Dense(nys_size, use_bias=False, activation='linear')(kernel_vector)  # 512 is the output dim of convolutional layers
    input_classifier = BatchNormalization()(input_classifier)
    input_classifier = Dropout(0.5)(input_classifier)

    classif = Dense(10, activation="softmax")(input_classifier)

    model = Model([input_x] + input_repr_subsample, [classif])
    # sgd = SGD(lr=.1, momentum=0.9, nesterov=True)
    adam = Adam(lr=.1)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

    def scheduler(epoch):
        if epoch < 50:
            return 1e-3
        if epoch < 100:
            return 1e-4
        return 1e-5

    change_lr = LearningRateScheduler(scheduler)
    cbks = [change_lr]
    # calcul de l'accuracy
    def gen():
        for k, (x_batch, y_batch) in enumerate(datagen.flow(x_train, y_train, batch_size=batch_size)):
            if x_batch.shape[0] != list_subsample_bases[0].shape[0]:
                continue
            yield [x_batch] + list_subsample_bases, y_batch


    model.fit_generator(gen(),
                    steps_per_epoch=int(x_train.shape[0]/batch_size),
                    epochs=epochs,
                    callbacks=cbks)

    def gen_eval():
        datagen = ImageDataGenerator()
        for k, (x_batch, y_batch) in enumerate(datagen.flow(x_test, y_test, batch_size=batch_size)):
            if x_batch.shape[0] != list_subsample_bases[0].shape[0]:
                continue
            yield [x_batch] + list_subsample_bases, y_batch


    print(model.evaluate_generator(gen_eval(), steps=int(x_test.shape[0]/batch_size)))

if __name__ == "__main__":

    try:
        main()
    except Exception as e:
        raise e
