import time as t

import numpy as np
import tensorflow as tf
import keras
from keras.layers import Dense, BatchNormalization, Flatten, Lambda
from keras.optimizers import SGD
from keras.preprocessing.image import ImageDataGenerator
import skluc.main.data.mldatasets as dataset
from skluc.main.data.utils import normalize, get_uniform_class_rand_indices
from skluc.main.tensorflow_.kernel_approximation.fastfood_layer import FastFoodLayer
from skluc.main.tensorflow_.kernel import tf_linear_kernel, tf_rbf_kernel, tf_chi_square_CPD, tf_chi_square_CPD_exp
from skluc.main.tensorflow_.utils import batch_generator
from skluc.main.utils import logger, memory_usage, ParameterManager, ResultManager, ResultPrinter, create_directory
from src import project_dir
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.initializers import he_normal


from src.features.build_features import _get_model_from_file


def build_sequential_model(input_shape):
    # build model
    model = Sequential()
    weight_decay = 0.0001

    # Block 1
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block1_conv1', input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block1_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'))

    # Block 2
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block2_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block2_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'))

    # Block 3
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'))

    # Block 4
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'))

    # Block 5
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool'))

    # model modification for cifar-10
    return model

def main():
    batch_size = 128
    epochs = 200
    num_classes = 10
    cifar10_data_dir = project_dir / "data/external" / "cifar10.npz"
    loaded_npz = np.load(cifar10_data_dir)
    (x_train, y_train), (x_test, y_test) = (loaded_npz["x_train"], loaded_npz["y_train"]), (loaded_npz["x_test"], loaded_npz["y_test"])

    # convmodel_func = _get_model_from_file("vgg19", "cifar10", "block5_pool")
    convmodel_func = build_sequential_model(x_train[0].shape)

    datagen = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=10000, random_state=0)

    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_val = keras.utils.to_categorical(y_val, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)
    x_train = x_train.astype('float32')
    x_val = x_val.astype('float32')
    x_test = x_test.astype('float32')
    y_train = y_train.astype('float32')
    y_val = y_val.astype('float32')
    y_test = y_test.astype('float32')
    x_train = normalize(x_train)  # type: np.ndarray
    x_val = normalize(x_val)  # type: np.ndarray
    x_test = normalize(x_test)  # type: np.ndarray

    datagen.fit(x_train)

    # # Model definition

    input_dim = x_train.shape[1:]
    output_dim = y_train.shape[1]

    x = tf.placeholder(tf.float32, shape=[None, *input_dim], name="x")
    y = tf.placeholder(tf.float32, shape=[None, output_dim], name="label")

    repr_x = convmodel_func(x) # convmodel_func will be reinitialized during the global variable initializer call
    repr_x = Flatten()(repr_x)

    dense_layer = Dense(512, activation="relu")
    input_classifier = dense_layer(repr_x)

    batchnorm_layer = BatchNormalization()
    input_classifier = batchnorm_layer(input_classifier)

    keep_prob = tf.placeholder(tf.float32)
    input_classifier = tf.nn.dropout(input_classifier, keep_prob)

    with tf.variable_scope("classification"):
        classif = Dense(output_dim)(input_classifier)

    # calcul de la loss
    with tf.name_scope("xent"):
        cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=classif, name="xentropy"),
            name="xentropy_mean")
        tf.summary.scalar('loss-xent', cross_entropy)

    # calcul du gradient
    with tf.name_scope("train"):
        global_step = tf.Variable(0, name="global_step", trainable=False)
        starter_learning_rate = 0.1
        # learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
        #                                            x_train.shape[0]/paraman["--batch-size"] * 80, 0.1, staircase=True)
        learning_rate = tf.placeholder(tf.float32, shape=[])
        # train_optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate,
        #                                              momentum=0.9, use_nesterov=True).minimize(cross_entropy,
        #                                                                          global_step=global_step)

        train_optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cross_entropy,
                                                                              global_step=global_step)


    # calcul de l'accuracy
    with tf.name_scope("accuracy"):
        predictions = tf.argmax(classif, 1)
        correct_prediction = tf.equal(predictions, tf.argmax(y, 1))
        accuracy_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar("accuracy", accuracy_op)

    merged_summary = tf.summary.merge_all()

    # In[6]:

    init = tf.global_variables_initializer()

    # In[7]:

    with tf.Session() as sess:
        logger.info("Start training")
        # Initialize all Variable objects
        sess.run(init)  # this reinitialize even the convolution layers
        # actual learning
        global_start = t.time()
        j = 0
        for i in range(epochs):
            logger.debug(memory_usage())
            if i < 50:
                learning_rate_val = 1e-3
            else:
                learning_rate_val = 1e-4

            for k, (x_batch, Y_batch) in enumerate(datagen.flow(x_train, y_train, batch_size=batch_size)):

                feed_dict = {x: x_batch, y: Y_batch, keep_prob:0.5, learning_rate:1e-3}
                _, loss, acc, summary_str = sess.run([train_optimizer, cross_entropy, accuracy_op, merged_summary], feed_dict=feed_dict)
                if j % 100 == 0:
                    logger.info(
                        "epoch: {}/{}; batch: {}/{}; batch_shape: {}; loss: {}; acc: {}".format(i, epochs,
                                                                                                k, int(x_train.shape[0] / batch_size) + 1,
                                                                                                x_batch.shape, loss,
                                                                                                acc))

                j += 1
                if k > int(x_train.shape[0] / batch_size):
                    break

        logger.info("Evaluation on validation data")
        training_time = t.time() - global_start
        accuracies_val = []
        i = 0
        val_eval_start = t.time()
        for x_batch, Y_batch in batch_generator(x_val, y_val, 1000, False):
            feed_dict = {x: x_batch, y: Y_batch, keep_prob:1.0}
            accuracy = sess.run([accuracy_op], feed_dict=feed_dict)
            accuracies_val.append(accuracy[0])
            i += 1
        global_acc_val = sum(accuracies_val) / i

        VAL_EVAL_TIME = t.time() - val_eval_start

        logger.info("Evaluation on test data")
        accuracies_test = []
        i = 0
        test_eval_start = t.time()
        for x_batch, Y_batch in batch_generator(x_test, y_test, 1000, False):
            feed_dict = {x: x_batch, y: Y_batch, keep_prob:1.0}
            accuracy = sess.run([accuracy_op], feed_dict=feed_dict)
            accuracies_test.append(accuracy[0])
            i += 1
        global_acc_test = sum(accuracies_test) / i
        TEST_EVAL_TIME = t.time() - test_eval_start

        print(global_acc_val, global_acc_test)


if __name__ == "__main__":

    try:
        main()
    except Exception as e:
        raise e
