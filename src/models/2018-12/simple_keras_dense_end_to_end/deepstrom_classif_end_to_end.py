import time as t

import numpy as np
import tensorflow as tf
import keras
from keras.callbacks import LearningRateScheduler
from keras.layers import Dense, BatchNormalization, Flatten, Lambda
from keras.optimizers import SGD, Adam
from keras.preprocessing.image import ImageDataGenerator
import skluc.main.data.mldatasets as dataset
from skluc.main.data.utils import normalize, get_uniform_class_rand_indices
from skluc.main.tensorflow_.kernel_approximation.fastfood_layer import FastFoodLayer
from skluc.main.tensorflow_.kernel import tf_linear_kernel, tf_rbf_kernel, tf_chi_square_CPD, tf_chi_square_CPD_exp
from skluc.main.tensorflow_.utils import batch_generator
from skluc.main.utils import logger, memory_usage, ParameterManager, ResultManager, ResultPrinter, create_directory
from src import project_dir
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.initializers import he_normal


from src.features.build_features import _get_model_from_file


def build_sequential_model(input_shape):
    # build model
    model = Sequential()
    weight_decay = 0.0001

    # Block 1
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block1_conv1', input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block1_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'))

    # Block 2
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block2_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block2_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'))

    # Block 3
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(256, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block3_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'))

    # Block 4
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block4_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'))

    # Block 5
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv1'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv2'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv3'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(512, (3, 3), padding='same', kernel_regularizer=keras.regularizers.l2(weight_decay), kernel_initializer=he_normal(), name='block5_conv4'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool'))

    # model modification for cifar-10
    return model

def main():
    batch_size = 128
    epochs = 200
    num_classes = 10
    cifar10_data_dir = project_dir / "data/external" / "cifar10.npz"
    loaded_npz = np.load(cifar10_data_dir)
    (x_train, y_train), (x_test, y_test) = (loaded_npz["x_train"], loaded_npz["y_train"]), (loaded_npz["x_test"], loaded_npz["y_test"])

    # convmodel_func = _get_model_from_file("vgg19", "cifar10", "block5_pool")
    convmodel_func = build_sequential_model(x_train[0].shape)

    datagen = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=10000, random_state=0)

    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_val = keras.utils.to_categorical(y_val, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)
    x_train = x_train.astype('float32')
    x_val = x_val.astype('float32')
    x_test = x_test.astype('float32')
    y_train = y_train.astype('float32')
    y_val = y_val.astype('float32')
    y_test = y_test.astype('float32')
    x_train = normalize(x_train)  # type: np.ndarray
    x_val = normalize(x_val)  # type: np.ndarray
    x_test = normalize(x_test)  # type: np.ndarray

    datagen.fit(x_train)

    # # Model definition

    input_dim = x_train.shape[1:]
    output_dim = y_train.shape[1]

    convmodel_func.add(Flatten())
    convmodel_func.add(Dense(512, activation="relu"))
    convmodel_func.add(BatchNormalization())
    convmodel_func.add(Dropout(0.5))
    convmodel_func.add(Dense(10, activation="softmax"))

    # sgd = SGD(lr=.1, momentum=0.9, nesterov=True)
    adam = Adam(lr=.1)
    convmodel_func.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

    def scheduler(epoch):
        if epoch < 50:
            return 1e-3
        if epoch < 100:
            return 1e-4
        return 1e-5

    change_lr = LearningRateScheduler(scheduler)
    cbks = [change_lr]
    # calcul de l'accuracy
    convmodel_func.fit_generator(datagen.flow(x_train, y_train,batch_size=batch_size),
                        steps_per_epoch=int(x_train.shape[0]/batch_size),
                        epochs=epochs,
                        callbacks=cbks,
                        validation_data=(x_val, y_val))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        raise e
