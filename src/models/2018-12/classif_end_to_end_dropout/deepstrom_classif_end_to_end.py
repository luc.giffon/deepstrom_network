"""
Benchmark VGG: Benchmarking deepstrom versus other architectures of the VGG network.

Usage:
    benchmark_classification dense [-q] [--cifar100|--cifar10|--mnist|--svhn] [-f name] [-t size] [-a value] [-v size] [-e numepoch] [-s batchsize] [-D reprdim] [-l size] [-V] [-d val] [-O name]
    benchmark_classification deepfriedconvnet [-q] [--cifar100|--cifar10|--mnist|--svhn] [-f name] [-t size] [-a value] [-v size] [-e numepoch] [-s batchsize] [-g gammavalue] [-N nbstack] [-l size] [-z] [-V] [-d val] [-O name]
    benchmark_classification deepstrom [-q] [--cifar100|--cifar10|--mnist|--svhn] [-f name] [-t size] [-r] [-a value] [-v size] [-e numepoch] [-s batchsize] [-D reprdim] [-m size] (-R|-L|-C|-E|-P|-S|-A|-T|-M) [-g gammavalue] [-c cvalue] [-n] [-l size] [-V] [-d val] [-O name]

Options:
    --help -h                               Display help and exit.
    -q --quiet                              Set logging level to info.
    -V --tensorboard                        Write tensorboard logs.
    -a --seed value                         The seed value used for all randomization processed [default: 0]
    -t --train-size size                    Size of train set.
    -v --validation-size size               The size of the validation set [default: 10000]
    -e --num-epoch=numepoch                 The number of epoch.
    -s --batch-size=batchsize               The number of example in each batch
    -d --dropout val                        Keep probability of neurons before classif [default: 1.0]
    -D reprdim --out-dim=reprdim            The dimension of the final representation
    -f --non-linearity name                 Tell the model which non-linearity to use when necessary (possible values: "relu", "tanh") [default: relu]
    -O --output-file name                   Tell if results should be written to disk and the name of the output file

Dense:
    -l --second-layer-size size             Says the size of the second non-linear layer [default: 0]

Deepfried convnet:
    -N nbstack --nb-stack nbstack           The number of fastfood stack for deepfriedconvnet
    -z --real-fastfood                      Tell fastfood layer to not update its weights

Deepstrom:
    -r --real-nystrom                       Says if the matrix for deepstrom should be K^(-1/2)
    -m size --nys-size size                 The number of example in the nystrom subsample.
    -n --non-linear                         Tell Nystrom to use the non linear activation function on its output.

Datasets:
    --cifar10                               Use cifar dataset
    --mnist                                 Use mnist dataset
    --svhn                                  Use svhn dataset
    --cifar100                              Use cifar100 dataset

Possible kernels:
    -R --rbf-kernel                         Says if the rbf kernel should be used for nystrom.
    -L --linear-kernel                      Says if the linear kernel should be used for nystrom.
    -C --chi-square-kernel                  Says if the basic additive chi square kernel should be used for nystrom.
    -E --exp-chi-square-kernel              Says if the exponential chi square kernel should be used for nystrom.
    -P --chi-square-PD-kernel               Says if the Positive definite version of the basic additive chi square kernel should be used for nystrom.
    -S --sigmoid-kernel                     Says it the sigmoid kernel should be used for nystrom.
    -A --laplacian-kernel                   Says if the laplacian kernel should be used for nystrom.
    -T --stacked-kernel                     Says if the kernels laplacian, chi2 and rbf in a stacked setting should be used for nystrom.
    -M --sumed-kernel                       Says if the kernels laplacian, chi2 and rbf in a summed setting should be used for nystrom.

Kernel related:
    -g gammavalue --gamma gammavalue        The value of gamma for rbf, chi or hyperbolic tangent kernel (deepstrom and deepfriedconvnet)
    -c cvalue --intercept-constant cvalue   The value of the intercept constant for the hyperbolic tangent kernel.

"""

import time as t

import docopt
import numpy as np
import tensorflow as tf
import keras
from keras.layers import Dense, BatchNormalization, Flatten, Lambda
from keras.optimizers import SGD
from keras.preprocessing.image import ImageDataGenerator
import skluc.main.data.mldatasets as dataset
from skluc.main.data.utils import normalize, get_uniform_class_rand_indices
from skluc.main.tensorflow_.kernel_approximation.fastfood_layer import FastFoodLayer
from skluc.main.tensorflow_.kernel import tf_linear_kernel, tf_rbf_kernel, tf_chi_square_CPD, tf_chi_square_CPD_exp
from skluc.main.tensorflow_.utils import batch_generator
from skluc.main.utils import logger, memory_usage, ParameterManager, ResultManager, ResultPrinter, create_directory
from src import project_dir
from sklearn.model_selection import train_test_split

from src.features.build_features import _get_model_from_file


class ParameterManagerMain(ParameterManager):

    def __init__(self, docopt_dict):
        super().__init__(docopt_dict)

        self["--out-dim"] = int(self["--out-dim"]) if eval(str(self["--out-dim"])) is not None else None
        self["kernel"] = self.init_kernel()
        self["network"] = self.init_network()
        self["activation_function"] = self.init_non_linearity()
        self["dataset"] = self.init_dataset()
        self["--nb-stack"] = int(self["--nb-stack"]) if self["--nb-stack"] is not None else None
        self["--nys-size"] = int(self["--nys-size"]) if self["--nys-size"] is not None else None
        self["--num-epoch"] = int(self["--num-epoch"])
        self["--validation-size"] = int(self["--validation-size"])
        self["--seed"] = int(self["--seed"])
        self["--batch-size"] = int(self["--batch-size"])
        self["deepstrom_activation"] = self.init_deepstrom_activation()
        self["--dropout"] = float(self["--dropout"])
        self["--train-size"] = int(self["--train-size"]) if self["--train-size"] is not None else None

        self.__kernel_dict = None

    def init_deepstrom_activation(self):
        if not self["deepstrom"]:
            return None

        if self["--non-linear"]:
            return self["--non-linearity"]
        else:
            return None

    def init_kernel_dict(self, data):
        if self["kernel"] == "rbf" or self["network"] == "deepfriedconvnet":
            GAMMA = self.get_gamma_value(data)
            self["--gamma"] = GAMMA
            self.__kernel_dict = {"gamma": GAMMA}
        elif self["kernel"] == "chi2_exp_cpd":
            GAMMA = self.get_gamma_value(data, chi2=True)
            self["--gamma"] = GAMMA
            self.__kernel_dict = {"gamma": GAMMA}
        elif self["kernel"] == "laplacian":
            GAMMA = self.get_gamma_value(data)
            self["--gamma"] = GAMMA
            self.__kernel_dict = {"gamma": np.sqrt(GAMMA)}
        else:
            self.__kernel_dict = {}

    def __getitem__(self, item):
        if item == "kernel_dict":
            return self.__kernel_dict
        else:
            return super().__getitem__(item)


class ResultManagerMain(ResultManager):
    def __init__(self):
        super().__init__()
        self["training_time"] = None
        self["val_eval_time"] = None
        self["val_acc"] = None
        self["test_acc"] = None
        self["test_eval_time"] = None


def main(paraman, resman, printman):
    if paraman["dataset"] == "mnist":
        num_classes = 10
        mnist_data_dir = project_dir / "data/external" / "mnist.npz"
        loaded_npz = np.load(mnist_data_dir)
        (x_train, y_train), (x_test, y_test) = (loaded_npz["x_train"], loaded_npz["y_train"]), (loaded_npz["x_test"], loaded_npz["y_test"])
        x_train = np.expand_dims(x_train, -1)
        x_test = np.expand_dims(x_test, -1)

        convmodel_func = _get_model_from_file("lenet", "mnist", "conv_pool_2")

        datagen = ImageDataGenerator(
            rotation_range=20,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=False)

    elif paraman["dataset"] == "cifar10":
        num_classes = 10
        cifar10_data_dir = project_dir / "data/external" / "cifar10.npz"
        loaded_npz = np.load(cifar10_data_dir)
        (x_train, y_train), (x_test, y_test) = (loaded_npz["x_train"], loaded_npz["y_train"]), (loaded_npz["x_test"], loaded_npz["y_test"])

        convmodel_func = _get_model_from_file("vgg19", "cifar10", "block5_pool")

        datagen = ImageDataGenerator(
            rotation_range=20,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=True)

    elif paraman["dataset"] == "cifar100":
        raise NotImplementedError
        data = dataset.Cifar100FineDataset(validation_size=paraman["--validation-size"], seed=paraman["--seed"])
        transformer = VGG19Transformer(data_name=paraman["dataset"], cut_layer_name="block5_pool")
        datagen = ImageDataGenerator(
            rotation_range=20,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=True)

    elif paraman["dataset"] == "svhn":
        num_classes = 10
        svhn_data_dir = project_dir / "data/external" / "svhn.npz"
        loaded_npz = np.load(svhn_data_dir)
        (x_train, y_train), (x_test, y_test) = (loaded_npz["x_train"], loaded_npz["y_train"]), (loaded_npz["x_test"], loaded_npz["y_test"])

        convmodel_func = _get_model_from_file("vgg19", "svhn", "block5_pool")

        datagen = ImageDataGenerator(
            rotation_range=20,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=False)

    else:
        raise ValueError("Unknown dataset")

    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=paraman["--validation-size"], random_state=0)

    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_val = keras.utils.to_categorical(y_val, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)
    x_train = x_train.astype('float32')
    x_val = x_val.astype('float32')
    x_test = x_test.astype('float32')
    y_train = y_train.astype('float32')
    y_val = y_val.astype('float32')
    y_test = y_test.astype('float32')
    x_train = normalize(x_train)  # type: np.ndarray
    x_val = normalize(x_val)  # type: np.ndarray
    x_test = normalize(x_test)  # type: np.ndarray


    if paraman["--train-size"] is not None:
        x_train, y_train = x_train[:paraman["--train-size"]], y_train[:paraman["--train-size"]]

    datagen.fit(x_train)

    paraman.init_kernel_dict(x_train)

    # # Model definition

    input_dim = x_train.shape[1:]
    output_dim = y_train.shape[1]

    x = tf.placeholder(tf.float32, shape=[None, *input_dim], name="x")
    y = tf.placeholder(tf.float32, shape=[None, output_dim], name="label")
    subs = tf.placeholder(tf.float32, shape=[paraman["--nys-size"], *input_dim], name="subsample")

    repr_x = convmodel_func(x) # convmodel_func will be reinitialized during the global variable initializer call
    repr_x = Flatten()(repr_x)
    # repr_x = batchnorm_layer(repr_x)
    repr_sub = convmodel_func(subs)
    repr_sub = Flatten()(repr_sub)
    # repr_sub = batchnorm_layer(repr_sub)

    logger.debug(paraman["kernel_dict"])

    if paraman["network"] == "deepstrom":
        if paraman["kernel"] == "linear":
            kernel_function = lambda input_list, *args, **kwargs: tf_linear_kernel(input_list[0], input_list[1])
        elif paraman["kernel"] == "rbf":
            kernel_function = lambda input_list, *args, **kwargs: tf_rbf_kernel(input_list[0], input_list[1], *args, **kwargs, **paraman["kernel_dict"])
        elif paraman["kernel"] == "chi2_cpd":
            kernel_function = lambda input_list, *args, **kwargs: tf_chi_square_CPD(input_list[0], input_list[1])
        elif paraman["kernel"] == "chi2_exp_cpd":
            kernel_function = lambda input_list, *args, **kwargs: tf_chi_square_CPD_exp(input_list[0], input_list[1], *args, **kwargs, **paraman["kernel_dict"])
            # elif paraman["kernel"] == "laplacian":
            #     kernel_function = lambda *args, **kwargs: tf_laplacian_kernel(*args, **kwargs, **paraman["kernel_dict"])
        else:
            raise NotImplementedError(f"unknown kernel function {paraman['kernel']}")

        kernel_layer = Lambda(kernel_function, output_shape=lambda shapes: (shapes[0][0], paraman["--nys-size"]))
        kernel_vector = kernel_layer([repr_x, repr_sub])
        input_classifier = Dense(paraman["--nys-size"], use_bias=False, activation='linear')(kernel_vector)  # 512 is the output dim of convolutional layers

        subsample_indexes = get_uniform_class_rand_indices(y_train, paraman["--nys-size"], seed=paraman["--seed"])
        nys_subsample = x_train[subsample_indexes]

    elif paraman["network"] == "dense":
        dense_layer = Dense(paraman["--out-dim"], activation=paraman["activation_function"])
        input_classifier = dense_layer(repr_x)
    elif paraman["network"] == "deepfriedconvnet":
        deepfried_layer = FastFoodLayer(sigma=1 / paraman["--gamma"], nbr_stack=paraman["--nb-stack"], trainable=not paraman["--real-fastfood"])
        input_classifier = deepfried_layer(repr_x)
    else:
        raise ValueError(f"Not recognized network {paraman['network']}")

    batchnorm_layer = BatchNormalization()
    input_classifier = batchnorm_layer(input_classifier)

    keep_prob = tf.placeholder(tf.float32)
    if paraman["--dropout"] is not None:
        input_classifier = tf.nn.dropout(input_classifier, keep_prob)

    with tf.variable_scope("classification"):
        classif = Dense(output_dim)(input_classifier)

    # calcul de la loss
    with tf.name_scope("xent"):
        cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=classif, name="xentropy"),
            name="xentropy_mean")
        tf.summary.scalar('loss-xent', cross_entropy)

    # calcul du gradient
    with tf.name_scope("train"):
        global_step = tf.Variable(0, name="global_step", trainable=False)
        starter_learning_rate = 0.1
        # learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
        #                                            x_train.shape[0]/paraman["--batch-size"] * 80, 0.1, staircase=True)
        learning_rate = tf.placeholder(tf.float32, shape=[])
        train_optimizer = tf.train.MomentumOptimizer(learning_rate=0.1,
                                                     momentum=0.9, use_nesterov=True).minimize(cross_entropy,
                                                                                 global_step=global_step)
        #
        # train_optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cross_entropy,
        #                                                                       global_step=global_step)


    # calcul de l'accuracy
    with tf.name_scope("accuracy"):
        predictions = tf.argmax(classif, 1)
        correct_prediction = tf.equal(predictions, tf.argmax(y, 1))
        accuracy_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar("accuracy", accuracy_op)

    merged_summary = tf.summary.merge_all()

    # In[6]:

    init = tf.global_variables_initializer()

    summary_writer = None
    if paraman["--tensorboard"]:
        summary_writer = tf.summary.FileWriter(f"log/{int(t.time())}/{paraman['dataset']}/nys_size_{paraman['--nys-size']}/")

    # In[7]:

    with tf.Session() as sess:
        logger.info("Start training")
        if paraman["--tensorboard"]:
            summary_writer.add_graph(sess.graph)
        # Initialize all Variable objects
        sess.run(init)  # this reinitialize even the convolution layers
        # actual learning
        global_start = t.time()
        j = 0
        for i in range(paraman["--num-epoch"]):
            logger.debug(memory_usage())
            if i < 80:
                learning_rate_val = 0.1
            elif i < 160:
                learning_rate_val = 0.01
            else:
                learning_rate_val = 0.001

            for k, (x_batch, Y_batch) in enumerate(datagen.flow(x_train, y_train, batch_size=paraman["--batch-size"])):

                if paraman["network"] == "deepstrom":
                    feed_dict = {x: x_batch, y: Y_batch, subs: nys_subsample, keep_prob:paraman["--dropout"], learning_rate:learning_rate_val}
                else:
                    feed_dict = {x: x_batch, y: Y_batch, keep_prob:paraman["--dropout"], learning_rate:learning_rate_val}
                _, loss, acc, summary_str = sess.run([train_optimizer, cross_entropy, accuracy_op, merged_summary], feed_dict=feed_dict)
                if j % 100 == 0:
                    logger.info(
                        "epoch: {}/{}; batch: {}/{}; batch_shape: {}; loss: {}; acc: {}".format(i, paraman["--num-epoch"],
                                                                                                k, int(x_train.shape[0] / paraman["--batch-size"]) + 1,
                                                                                                x_batch.shape, loss,
                                                                                                acc))
                    if paraman["--tensorboard"]:
                        summary_writer.add_summary(summary_str, j)
                j += 1
                if k > int(x_train.shape[0] / paraman["--batch-size"]):
                    break

        logger.info("Evaluation on validation data")
        training_time = t.time() - global_start
        resman["training_time"] = training_time
        accuracies_val = []
        i = 0
        val_eval_start = t.time()
        for x_batch, Y_batch in batch_generator(x_val, y_val, 1000, False):
            if paraman["network"] == "deepstrom":
                feed_dict = {x: x_batch, y: Y_batch, subs: nys_subsample, keep_prob:1.0}
            else:
                feed_dict = {x: x_batch, y: Y_batch, keep_prob:1.0}
            accuracy = sess.run([accuracy_op], feed_dict=feed_dict)
            accuracies_val.append(accuracy[0])
            i += 1
        global_acc_val = sum(accuracies_val) / i

        VAL_EVAL_TIME = t.time() - val_eval_start
        resman["val_eval_time"] = VAL_EVAL_TIME
        resman["val_acc"] = global_acc_val

        logger.info("Evaluation on test data")
        accuracies_test = []
        i = 0
        test_eval_start = t.time()
        for x_batch, Y_batch in batch_generator(x_test, y_test, 1000, False):
            if paraman["network"] == "deepstrom":
                feed_dict = {x: x_batch, y: Y_batch, subs: nys_subsample, keep_prob:1.0}
            else:
                feed_dict = {x: x_batch, y: Y_batch, keep_prob:1.0}
            accuracy = sess.run([accuracy_op], feed_dict=feed_dict)
            accuracies_test.append(accuracy[0])
            i += 1
        global_acc_test = sum(accuracies_test) / i
        TEST_EVAL_TIME = t.time() - test_eval_start
        resman["test_acc"] = global_acc_test
        resman["test_eval_time"] = TEST_EVAL_TIME
        printman.print()


if __name__ == "__main__":
    paraman_obj = ParameterManagerMain(docopt.docopt(__doc__))
    resman_obj = ResultManagerMain()
    # output_dir = project_dir / "results/2018-12/classif_end_to_end_dropout"
    # create_directory(output_dir)
    printman_obj = ResultPrinter(paraman_obj, resman_obj)
                                 # output_file=output_dir / (paraman_obj["--output-file"] + "_" + str(int(t.time())) if paraman_obj["--output-file"] is not None else None))

    try:
        main(paraman_obj, resman_obj, printman_obj)
    except Exception as e:
        printman_obj.print()
        raise e
