
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pathlib
import os

from skluc.main.utils import logger

matplotlib.rcParams.update({'font.size': 14})
pd.set_option('display.expand_frame_repr', False)


# In[2]:


def build_df(dirname, filename):
    filepath = os.path.join(dirname, filename)
    df = pd.read_csv(filepath)
    df = df.apply(pd.to_numeric, errors="ignore")
    df = df.drop_duplicates()
    col_to_delete = ['--chi-square-PD-kernel', '--chi-square-kernel',
       '--cifar10', '--cifar100', '--exp-chi-square-kernel',
       '--intercept-constant', '--laplacian-kernel', '--linear-kernel',
       '--mnist', '--quiet', '--rbf-kernel',
       '--sigmoid-kernel',  '--svhn',
       '--tensorboard', '--validation-size'
       , 'deepstrom','dense', 
                     "--non-linear", "--non-linearity", "--num-epoch",
                     "--train-size", "--second-layer-size", "activation_function",
                     "deepstrom_activation", "--real-nystrom"
            ]
    for c in col_to_delete:
        df = df.drop([c], axis=1)
    return df


# In[11]:


DIRNAME = "/home/luc/PycharmProjects/deepstrom_network/results/2019-01/classif_end_to_end_deepstrom_svhn_after_grid/"
FILENAME = "gathered_results.csv"
df_deepstrom_grid = build_df(DIRNAME, FILENAME)


# In[21]:





# In[12]:


DIRNAME = "/home/luc/PycharmProjects/deepstrom_network/results/2019-01/classif_end_to_end_deepstrom_mnist_svhn/"
FILENAME = "gathered_results.csv"
df_deepstrom = build_df(DIRNAME, FILENAME)
df_deepstrom = df_deepstrom[df_deepstrom["dataset"] == "svhn"]


# In[13]:


DIRNAME = "/home/luc/PycharmProjects/deepstrom_network/results/2019-01/classif_end_to_end_dense_mnist_svhn/"
FILENAME = "gathered_results.csv"
df_dense = build_df(DIRNAME, FILENAME)
df_dense = df_dense[df_dense["dataset"] == "svhn"]


# In[14]:


def get_sorted_acc_for_dataset(df_, name):
    df_dataset = df_[df_["dataset"] == name]
    df_dataset = df_dataset.sort_values(by="val_acc", ascending=False)
    return df_dataset


# In[23]:


nb_classes_datasets = {
    "svhn": 10,
    "cifar10": 10,
    "mnist": 10,
    "cifar100": 100
}

nb_feature_convs = {
    "svhn": 512,
    "cifar10": 512,
    "mnist": 16,
    "cifar100": 512
}

min_acc = 0.5
max_acc = 1.1
DATANAME = "svhn"


# In[24]:


def post_processing_figures(f, ax, nbparamdeepstrom, subsample_sizes, name):
    ax.set_ylim(min_acc, max_acc)
    ax.set_ylabel("Accuracy")
    ax.set_xticks([1e4, 1e5, 1e6])
    ax.set_xlabel("# Learnable Parameters")
    ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=2)
    ax.set_xticklabels([1e4, 1e5, 1e6])
    ax.set_xscale("symlog")

    ax_twin = ax.twiny()
    ax_twin.set_xscale("symlog")
    ax_twin.set_xlim(ax.get_xlim())
    ax_twin.set_xticks(sorted(nbparamdeepstrom))
    ax_twin.set_xticklabels(sorted(subsample_sizes))
    ax_twin.set_xlabel("Subsample Size")
    ax.set_title("{}".format(DATANAME), y=1.2)

    f.set_size_inches(8, 6)
    f.tight_layout()
    f.subplots_adjust(bottom=0.3)

    out_name = "end_to_end_{}_{}".format(DATANAME, name)
    # return
    base_out_dir = os.path.abspath(__file__.split(".")[0])
    base_out_dir_path = pathlib.Path(base_out_dir) / "images"
    base_out_dir_path.mkdir(parents=True, exist_ok=True)
    out_path = base_out_dir_path / out_name
    logger.debug(out_path)
    f.savefig(out_path)


# In[32]:


def create_figure_from_df_deepstrom(df_deeps, df_dense, name_fig):
    kernel_names = set(df_deeps["kernel"].values)
    nys_size = set(df_deeps["--nys-size"].values)
    gamma_values = set(df_deeps["--gamma"].values)
    gamma_values.remove("None")

    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    # display(df_deeps)
    df_data = df_deeps
    nb_classes_dataset = nb_classes_datasets[DATANAME]
    nb_feature_conv = nb_feature_convs[DATANAME]
    f, ax = plt.subplots()
    for k_name in kernel_names:
        df_kernel = df_data[df_data["kernel"] == k_name]
        df_kernel = df_kernel.sort_values(by=["--nys-size"])
        if k_name == "rbf":
            for g_val in gamma_values:
                k_name = "rbf" + "_" + str(g_val)
                df_rbf = df_kernel[df_kernel["--gamma"] == g_val]
                # display(df_rbf)
                accuracies_kernel_mean = np.mean(
        np.array([list(df_rbf[df_rbf["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)
                
                accuracies_kernel_std = np.std(
        np.array([list(df_rbf[df_rbf["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)
                
                subsample_sizes_kernel = np.array(sorted([int(n) for n in np.unique(df_rbf["--nys-size"])]))
                np_param = (np.square(subsample_sizes_kernel) +  # m x m
                            subsample_sizes_kernel * nb_classes_dataset)  # m x c
                                
                xx = np_param
                yy = accuracies_kernel_mean.astype(float)
                ax.plot(xx, yy, marker="x", label=f"Deepstrom {k_name}")
        else:
            accuracies_kernel_mean = np.mean(
        np.array([list(df_kernel[df_kernel["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)
            
            accuracies_kernel_std = np.std(
        np.array([list(df_kernel[df_kernel["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)

            subsample_sizes_kernel = np.array(sorted([int(n) for n in np.unique(df_kernel["--nys-size"])]))
            
            np_param = (np.square(subsample_sizes_kernel) +  # m x m
                        subsample_sizes_kernel * nb_classes_dataset)  # m x c

            xx = np_param
            yy = accuracies_kernel_mean.astype(float)
            ax.plot(xx, yy, marker="x", label=f"Deepstrom {k_name}")

    df_dense["--out-dim"] = df_dense["--out-dim"].astype(np.int)
    df_dense = df_dense.sort_values(by=["--out-dim"])
    accuracies_dense_mean = np.mean(
        np.array([list(df_dense[df_dense["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)
    accuracies_dense_std = np.std(
        np.array([list(df_dense[df_dense["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)
    
    out_dim_dense = np.array(sorted([int(n) for n in np.unique(df_dense["--out-dim"])]))
    np_param_dense = (nb_feature_conv * out_dim_dense +  # d x D
                out_dim_dense * nb_classes_dataset)  # D x c
    
    
#     display(accuracies_dense_mean)
#     display(np_param_dense)
    xx = np_param_dense
    # display(xx)
    yy = accuracies_dense_mean.astype(float)
    # display(yy)
    ax.plot(xx, yy, marker="o", label=f"Dense")

    post_processing_figures(f, ax, np_param, subsample_sizes_kernel, name_fig)


# In[30]:


# lst_df_kernel = []
# kernel_names = set(df_deepstrom_grid["kernel"].values)
# nys_size = set(df_deepstrom_grid["--nys-size"].values)

# for k_name in kernel_names:
#     df_k = df_deepstrom_grid[df_deepstrom_grid["kernel"] == k_name]
#     df_k_sorted = get_sorted_acc_for_dataset(df_k, "mnist")
#     lst_df_kernel.append(df_k_sorted.iloc[0:1])

# df_deepstrom_grid_processed = pd.concat(lst_df_kernel)
                         
create_figure_from_df_deepstrom(df_deepstrom_grid, df_dense, "after_grid_search")


# In[34]:


# lst_df_kernel = []
# kernel_names = set(df_deepstrom_grid["kernel"].values)
# nys_size = set(df_deepstrom_grid["--nys-size"].values)

# for k_name in kernel_names:
#     df_k = df_deepstrom_grid[df_deepstrom_grid["kernel"] == k_name]
#     df_k_sorted = get_sorted_acc_for_dataset(df_k, "mnist")
#     lst_df_kernel.append(df_k_sorted.iloc[0:1])

# df_deepstrom_grid_processed = pd.concat(lst_df_kernel)
def create_figure_from_df_deepstrom(df_deeps, df_dense, name_fig):
    kernel_names = set(df_deeps["kernel"].values)
    nys_size = set(df_deeps["--nys-size"].values)
    gamma_values = set(df_deeps["--gamma"].values)
    gamma_values.remove("None")

    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    # display(df_deeps)
    df_data = df_deeps
    nb_classes_dataset = nb_classes_datasets[DATANAME]
    nb_feature_conv = nb_feature_convs[DATANAME]
    f, ax = plt.subplots()
    for k_name in kernel_names:
        df_kernel = df_data[df_data["kernel"] == k_name]
        if k_name == "rbf":
            for g_val in gamma_values:
                k_name = "rbf" + "_" + str(g_val)
                df_rbf = df_kernel[df_kernel["--gamma"] == g_val]
                accuracies_kernel = df_rbf["test_acc"]
                subsample_sizes_kernel = df_rbf["--nys-size"].astype(int)
                np_param = (np.square(subsample_sizes_kernel) +  # m x m
                            subsample_sizes_kernel * nb_classes_dataset)  # m x c
                sorted_idx = np.argsort(np_param.values)
                xx = np_param.values[sorted_idx]
                print(accuracies_kernel.values[sorted_idx])
                yy = accuracies_kernel.values[sorted_idx].astype(float)
                ax.plot(xx, yy, marker="x", label=f"Deepstrom {k_name}")
        else:
            accuracies_kernel = df_kernel["test_acc"]
            subsample_sizes_kernel = df_kernel["--nys-size"].astype(int)
            np_param = (np.square(subsample_sizes_kernel) +  # m x m
                        subsample_sizes_kernel * nb_classes_dataset)  # m x c
            sorted_idx = np.argsort(np_param.values)
            xx = np_param.values[sorted_idx]
            print(accuracies_kernel.values[sorted_idx])
            yy = accuracies_kernel.values[sorted_idx].astype(float)
            ax.plot(xx, yy, marker="x", label=f"Deepstrom {k_name}")

    df_dense["--out-dim"] = df_dense["--out-dim"].astype(np.int)
    df_dense = df_dense.sort_values(by=["--out-dim"])
    accuracies_dense_mean = np.mean(
        np.array([list(df_dense[df_dense["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)
    accuracies_dense_std = np.std(
        np.array([list(df_dense[df_dense["--seed"] == seed_v]["test_acc"]) for seed_v in
                  range(5)]), axis=0)
    
    out_dim_dense = np.array(sorted([int(n) for n in np.unique(df_dense["--out-dim"])]))
    np_param_dense = (nb_feature_conv * out_dim_dense +  # d x D
                out_dim_dense * nb_classes_dataset)  # D x c
    
    
#     display(accuracies_dense_mean)
#     display(np_param_dense)
    xx = np_param_dense
    # display(xx)
    yy = accuracies_dense_mean.astype(float)
    # display(yy)
    ax.plot(xx, yy, marker="o", label=f"Dense")

    post_processing_figures(f, ax, np_param, subsample_sizes_kernel, name_fig)


create_figure_from_df_deepstrom(df_deepstrom, df_dense, "normal")

