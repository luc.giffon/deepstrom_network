
# coding: utf-8

# In[53]:


import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pathlib
import os
from src import project_dir

from skluc.main.utils import logger

matplotlib.rcParams.update({'font.size': 14})
pd.set_option('display.expand_frame_repr', False)


# In[54]:


def build_df(dirname, filename):
    filepath = os.path.join(dirname, filename)
    df = pd.read_csv(filepath)
    df = df.apply(pd.to_numeric, errors="ignore")
    df = df.drop_duplicates()
    col_to_delete = ['--batch-size', '--chi-square-PD-kernel', '--chi-square-kernel',
       '--cifar10', '--cifar100', '--exp-chi-square-kernel',
       '--intercept-constant', '--laplacian-kernel', '--linear-kernel',
       '--mnist', '--quiet', '--rbf-kernel',
       '--sigmoid-kernel', '--stacked-kernel', '--sumed-kernel', '--svhn',
       '--tensorboard', '--validation-size', "--out-dim", "dataset",
       'deepfriedconvnet', 'deepstrom','dense', "--nb-stack", 
                     "--non-linear", "--non-linearity", "--num-epoch", "--seed",
                     "--train-size", "--second-layer-size", "activation_function",
                     "deepstrom_activation", "--real-fastfood", "--real-nystrom"
            ]
    for c in col_to_delete:
        df = df.drop([c], axis=1)
    return df


# In[55]:


DIRNAME = "/home/luc/PycharmProjects/deepstrom_network/results/2019-01/classif_end_to_end_deepstrom/gathered/"
FILENAME = "gathered_results.csv"
df_small_subsample = build_df(DIRNAME, FILENAME)
df_small_subsample = df_small_subsample[df_small_subsample["--nys-size"] != 64]


# In[56]:


DIRNAME = "/home/luc/PycharmProjects/deepstrom_network/results/2019-01/classif_end_to_end_deepstrom_bigger_subsample/"
FILENAME = "gathered_results.csv"
df_big_subsample = build_df(DIRNAME, FILENAME)


# In[57]:


df = pd.concat([df_small_subsample, df_big_subsample], axis=0, ignore_index=True, sort=True)


# In[58]:


def get_sorted_acc_for_dataset(df_):
    df_dataset = df_
    df_dataset = df_dataset.sort_values(by="val_acc", ascending=False)
    return df_dataset


# In[59]:


get_sorted_acc_for_dataset(df)


# In[61]:


method_names = set(df["network"].values)
kernel_names = set(df["kernel"].values)
nys_size = set(df["--nys-size"].values)
gamma_values = set(df["--gamma"].values)
gamma_values.remove("None")

logger.debug("Nystrom possible sizes are: {}".format(nys_size))
logger.debug("Kernel functions are: {}".format(kernel_names))
logger.debug("Compared network types are: {}".format(method_names))


# In[62]:


nb_classes_datasets = {
    "cifar10": 10,
}

nb_feature_convs = {
    "cifar10": 512,
}

DATANAME = "cifar10"
min_acc = 0
max_acc = 1


# In[63]:


def post_processing_figures(f, ax, nbparamdeepstrom, subsample_sizes):
    ax.set_ylim(min_acc, max_acc)
    ax.set_ylabel("Accuracy")
    ax.set_xticks([1e4, 1e5, 1e6])
    ax.set_xlabel("# Learnable Parameters")
    ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=2)
    ax.set_xticklabels([1e4, 1e5, 1e6])
    ax.set_xscale("symlog")

    ax_twin = ax.twiny()
    ax_twin.set_xscale("symlog")
    ax_twin.set_xlim(ax.get_xlim())
    ax_twin.set_xticks(sorted(nbparamdeepstrom))
    ax_twin.set_xticklabels(sorted(subsample_sizes))
    ax_twin.set_xlabel("Subsample Size")
    ax.set_title("{}".format(DATANAME), y=1.2)

    f.set_size_inches(8, 6)
    f.tight_layout()
    f.subplots_adjust(bottom=0.3)

    out_name = "end_to_end_{}".format(DATANAME)
    # return
    path_date_name = os.path.abspath(__file__).split("/")[-3:-1]
    base_out_dir = project_dir / "reports" / "figures" / path_date_name
    base_out_dir_path = pathlib.Path(base_out_dir)
    base_out_dir_path.mkdir(parents=True, exist_ok=True)
    out_path = base_out_dir_path / out_name
    logger.debug(out_path)
    f.savefig(out_path)


# In[66]:


df_data = df
nb_classes_dataset = nb_classes_datasets[DATANAME]
nb_feature_conv = nb_feature_convs[DATANAME]
f, ax = plt.subplots()
for k_name in kernel_names:
    df_kernel = df_data[df_data["kernel"] == k_name]
    if k_name == "rbf":
        for g_val in gamma_values:
            k_name = "rbf" + "_" + str(g_val)
            df_rbf = df_kernel[df_kernel["--gamma"] == g_val]
            accuracies_kernel = df_rbf["test_acc"]
            subsample_sizes_kernel = df_rbf["--nys-size"].astype(int)
            np_param = (np.square(subsample_sizes_kernel) +  # m x m
                        subsample_sizes_kernel * nb_classes_dataset)  # m x c
            sorted_idx = np.argsort(np_param.values)
            xx = np_param.values[sorted_idx]
            print(accuracies_kernel.values[sorted_idx])
            yy = accuracies_kernel.values[sorted_idx].astype(float)
            ax.plot(xx, yy, marker="x", label=f"Deepstrom {k_name}")
    else:
        accuracies_kernel = df_kernel["test_acc"]
        subsample_sizes_kernel = df_kernel["--nys-size"].astype(int)
        np_param = (np.square(subsample_sizes_kernel) +  # m x m
                    subsample_sizes_kernel * nb_classes_dataset)  # m x c
        sorted_idx = np.argsort(np_param.values)
        xx = np_param.values[sorted_idx]
        print(accuracies_kernel.values[sorted_idx])
        yy = accuracies_kernel.values[sorted_idx].astype(float)
        ax.plot(xx, yy, marker="x", label=f"Deepstrom {k_name}")
    

post_processing_figures(f, ax, np_param, subsample_sizes_kernel)

