# -*- coding: utf-8 -*-
import click
import logging
import os
import numpy as np
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

from skluc.main.data.utils import normalize
from skluc.main.utils import logger, silentremove, download_data, check_file_md5, DownloadableModel, create_directory
from keras import Model
from keras.models import load_model

from src import project_dir


def _get_model_from_file(architecture, weights, cut_layer):
    model_directory_path = project_dir / "models/external" / architecture / weights
    logger.debug(f"Model located at {model_directory_path}")
    keras_model = load_model(str(model_directory_path / os.listdir(model_directory_path)[-1]))
    if cut_layer is None:
        cut_layer = keras_model.layers[-1].name
    keras_model = Model(inputs=keras_model.input,
                        outputs=keras_model.get_layer(name=cut_layer).output)
    return keras_model

@click.command()
@click.argument('dataset', default="all")
@click.argument('architecture', type=click.Path())
@click.argument('cut_layer', type=click.Path(), default="None")
@click.argument('weights', type=click.Path())
@click.argument('output_dirpath', type=click.Path())

def main(output_dirpath, dataset, architecture, weights, cut_layer):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    if cut_layer == "None":
        cut_layer = None
    keras_model = _get_model_from_file(architecture, weights, cut_layer)

    input_dataset_directory_path = project_dir / "data/external" / f"{dataset}.npz"

    loaded_npz = np.load(input_dataset_directory_path)
    (x_train, y_train), (x_test, y_test) = (loaded_npz["x_train"], loaded_npz["y_train"]), (loaded_npz["x_test"], loaded_npz["y_test"])

    logger.debug(f"Load saved data at {input_dataset_directory_path}: {x_train.shape}, {y_train.shape}, {x_test.shape}, {y_test.shape}")

    x_train = normalize(x_train)  # type: np.ndarray
    x_test = normalize(x_test)  # type: np.ndarray

    if dataset == "mnist":
        x_train = np.expand_dims(x_train, -1)
        x_test = np.expand_dims(x_test, -1)

    x_train = np.array(keras_model.predict(x_train)).reshape(-1, *keras_model.output_shape[1:])
    x_test = np.array(keras_model.predict(x_test)).reshape(-1, *keras_model.output_shape[1:])

    logger.debug(f"Shape of transformed data: {x_train.shape}, {y_train.shape}, {x_test.shape}, {y_test.shape}")

    map_savez = {"x_train": x_train,
                 "y_train": y_train,
                 "x_test": x_test,
                 "y_test": y_test
                 }

    output_path = project_dir / output_dirpath / architecture / cut_layer
    create_directory(output_path)
    np.savez(output_path / dataset, **map_savez)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
